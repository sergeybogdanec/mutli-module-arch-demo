include(":features:settings")
include(":features:auth")
include(
    ":accounting",
    ":store",
    ":accountingdata",
    ":storedata"
)
rootProject.name = "multi-module-arch-demo"