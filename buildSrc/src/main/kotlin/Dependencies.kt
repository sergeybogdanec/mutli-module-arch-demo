object CommonVersions {
    const val accountingModuleVersionName = "1.0"
    const val accountingModuleVersionCode = 1

    const val storeModuleVersionName = "1.0"
    const val storeModuleVersionCode = 1

    const val kotlin = "1.3.72"
    const val gradleBuildToolsVersion = "4.1.0"
    const val androidBuildToolsVersion = "30.0.2"

    const val jvmTarget = "1.8"
}

object AndroidSDK {
    const val min = 23
    const val compile = 30
    const val target = 30
}

object BuildPlugins {
    const val androidGradlePlugin = "com.android.tools.build:gradle:${CommonVersions.gradleBuildToolsVersion}"
    const val kotlinGradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${CommonVersions.kotlin}"
    const val androidApplication = "com.android.application"
    const val kotlinAndroid = "org.jetbrains.kotlin.android"
    const val kotlinAndroidExtensions = "org.jetbrains.kotlin.android.extensions"
    const val dynamicFeature = "com.android.dynamic-feature"
    const val androidLibrary = "com.android.library"
    const val kapt = "kotlin-kapt"
}

object Libraries {

    object Versions {
        const val androidxCore = "1.3.2"
        const val appCompat = "1.2.0"
        const val material = "1.2.1"
        const val constraintsLayout = "2.0.4"
    }

    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib:${CommonVersions.kotlin}"
    const val androidxCore = "androidx.core:core-ktx:${Versions.androidxCore}"
    const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    const val material = "com.google.android.material:material:${Versions.material}"
    const val constraintsLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintsLayout}"

}

object TestLibraries {

    object Versions {
        const val junit = "4.+"
        const val androidJunit = "1.1.2"
        const val espresso = "3.3.0"
    }

    const val junit = "junit:junit:${Versions.junit}"
    const val androidJunit = "androidx.test.ext:junit:${Versions.androidJunit}"
    const val espresso = "androidx.test.espresso:espresso-core:${Versions.espresso}"
}

object Modules {
    object Applications {
        const val store = ":store"
        const val accounting = ":accounting"
    }
    object Data {
        const val storeData = ":storeData"
        const val accountingData = ":accountingData"
    }
    object Features {
        private const val features = ":features"
        const val auth = "$features:auth"
        const val settings = "$features:settings"
    }
}
